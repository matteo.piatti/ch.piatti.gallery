var express = require('express')
var path = require('path')
var favicon = require('serve-favicon')
var logger = require('morgan')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
var nodemailer = require('nodemailer')
var routing = require('./routes/routing.js')
var https = require('https')
var database = require('./lib/db/database.js')
var fileUpload = require('express-fileupload')

var app = express()

database.download()

// lowercase everything
app.use(require('express-uncapitalize')())
app.use(fileUpload())

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public'), {}))

app.post('/kontakt', function (req, res) {
  var captchaError, nameError, mailError, subjectError, areaError, contactTitle, errorMessage

  verifyRecaptcha(req.body['g-recaptcha-response'], function (success, response) {
    if (success) {
      if (req.body.name === '' || /^[0-9-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]+$/.test(req.body.name)) { // eslint-disable-line no-useless-escape
        nameError = 'error'
      }
      if (req.body.email === '' || !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(req.body.email)) { // eslint-disable-line no-useless-escape
        mailError = 'error'
      }
      console.log(req.body.message)
      if (req.body.message === '') {
        areaError = 'error'
      }
      if (req.body.subject === '') {
        subjectError = 'error'
      }

      if (mailError === 'error' || nameError === 'error' || subjectError === 'error' || areaError === 'error') {
        contactTitle = 'Überprüfen sie die falschen Felder'
        errorMessage = true
        sendContactSite()
      } else {
        captchaError = 'success'

        var mailOpts, smtpTrans
        var d = new Date()
        // Setup Nodemailer transport, I chose gmail. Create an application-specific password to avoid problems.
        smtpTrans = nodemailer.createTransport({
          service: 'Gmail',
          auth: {
            user: 'galerie.redirect@gmail.com',
            pass: 'JWrtsj6&zZx;4cm'
          }
        })
        // Mail options
        mailOpts = {
          from: req.body.name + ' &lt;' + req.body.email + '&gt;', // grab form data from the request body object
          to: 'matteo.piatti@liip.ch',
          subject: req.body.subject,
          text: '[Nachricht von ' + req.body.name + ' am ' + d + ']\n' +
          '!--------~NACHRICHT ANFANG~--------!\n\n' +
          req.body.message +
          '\n\n!--------~NACHRICHT ENDE~--------!'
        }
        smtpTrans.sendMail(mailOpts, function (error, response) {
          if (error) {
            contactTitle = 'Fehler, Nachricht nicht gesendet'
            errorMessage = true
            sendContactSite()
          } else {
            contactTitle = 'Nachricht gesendet'
            errorMessage = false
            sendContactSite()
          }
        })
      }
    } else {
      captchaError = 'error'
      contactTitle = 'Captcha Fehler, bitte verifizieren Sie sich erneut'
      errorMessage = true
      sendContactSite()
    }
  })
  function sendContactSite () {
    res.render('contact', { title: contactTitle, name: 'contact', subject: req.body.subject, field: req.body.message, mail: req.body.email, pers: req.body.name, error: { captcha: captchaError, name: nameError, mail: mailError, subject: subjectError, area: areaError }, showErrorMessage: errorMessage })
  }
})

app.post('/upload', function (req, res) {
  if (!req.files) {
    return res.status(400).send('No files were uploaded.')
  }

  var sampleFile = req.files.file

  // Use the mv() method to place the file somewhere on your server
  sampleFile.mv('./public/upload/' + req.files.file.name , function (err) {
    if (err) {
      return res.status(500).send(err)
    }
    return res.redirect(200, '/admin')
  })
  database.upload('./public/upload/' + req.files.file.name, req.body)
})

app.post('/update', function (req, res) {
  database.download()
})

app.post('/delete', function (req, res) {
  console.log(req.body.id)
  database.delete(req.body.id)
})

app.all('*', routing)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

function verifyRecaptcha (key, callback) {
  https.get('https://www.google.com/recaptcha/api/siteverify?secret=6LcwJjEUAAAAACSC7gF9GtGCZj7sTTrcITdbgzQ1&response=' + key, function (res) {
    var data = ''
    res.on('data', function (chunk) {
      data += chunk.toString()
    })
    res.on('end', function () {
      try {
        var parsedData = JSON.parse(data)
        console.log(parsedData)
        callback(parsedData.success)
      } catch (e) {
        callback(e, false)
      }
    })
  })
}

module.exports = app
