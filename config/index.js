/*
var dropboxAccessToken = process.env.DROPBOX_ACCESS_TOKEN

if (!dropboxAccessToken) {
  throw new Error('No DROPBOX_ACCESS_TOKEN given')
}
*/

var cloudName = process.env.CLOUD_NAME
var apiKey = process.env.API_KEY
var apiSecret = process.env.API_SECRET

var paintingsFallback = {
  title: 'Untitled Painting'
}

var sketchesFallback = {
  title: 'Untitled Painting'
}

module.exports = {
  paintingsFallback: paintingsFallback,
  sketchesFallback: sketchesFallback,
  cloudName: cloudName,
  apiKey: apiKey,
  apiSecret: apiSecret
}
