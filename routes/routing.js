var express = require('express')
var router = express.Router()
var database = require('../lib/db/database.js')
var data = database.data

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Piatti Galerie', name: 'home' })
})

router.get('/home', function (req, res, next) {
  res.render('index', { title: 'Piatti Galerie', name: 'home' })
})

/* GET contact page. */
router.get('/kontakt', function (req, res, next) {
  var subject = ''
  var text = ''
  res.render('contact', { title: 'Piatti Galerie', name: 'contact', subject: subject, field: text, error: 'false' })
})

/* GET about page. */
router.get('/about', function (req, res, next) {
  res.render('about', { title: 'Piatti Galerie', name: 'about' })
})

/* GET gallery page. */
router.get('/galerie', function (req, res, next) {
  res.render('gallery', { title: 'Piatti Galerie', name: 'galerie', routed: false, paintings: data.paintings })
})

router.get('/galerie/:id', function (req, res, next) {
  var image
  data.paintings.forEach(function (entry, key) {
    if (data.paintings[key].folder === req.params.id) {
      image = data.paintings[key]
    }
  })

  if (image) {
    res.render('image', { title: 'Piatti Galerie', name: 'galerie', id: req.params.id, image: image })
  } else {
    res.status(404).render('gallery', { title: 'Piatti Galerie', name: 'galerie', id: req.params.id, routed: true, paintings: data.paintings })
  }
})

/* GET sketches page. */
router.get('/skizzen', function (req, res, next) {
  res.render('sketches', { title: 'Piatti Galerie', name: 'skizzen', routed: false, sketches: data.sketches })
})

router.get('/skizzen/:id', function (req, res, next) {
  var sketch
  data.sketches.forEach(function (entry, key) {
    if (data.sketches[key].folder === req.params.id) {
      sketch = data.sketches[key]
    }
  })

  if (sketch) {
    res.render('sketch', { title: 'Piatti Galerie', name: 'skizzen', id: req.params.id, sketch: sketch })
  } else {
    res.status(404).render('sketches', { title: 'Piatti Galerie', name: 'skizzen', id: req.params.id, routed: true, sketches: data.sketches })
  }
})

router.get('/admin', function (req, res, next) {
  res.render('admin', { title: 'ADMIN', name: 'admin', paintings: data.paintings, sketches: data.sketches})
  console.log(req.file)
})


/* GET json page. Only needed for developing reasons */
router.get('/db/name.json', function (req, res) {
  res.json(data)
})

/* Everything else is 404 */
router.get('*', function (req, res) {
  res.status(404).render('404')
})

module.exports = router
