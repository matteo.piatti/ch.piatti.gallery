var element = document.getElementById('hide')

if ($(window).width() < 700) {
  $(document).ready(function () {
    var contentPlacement = $('.navigation').position().top + $('.navigation').height()
    $('.content').css('margin-top', contentPlacement)
    $('.menuList').css('margin-top', contentPlacement)
    $('#hide').css('margin-top', contentPlacement)
  })
}

$('.menu').blur(function () {
  openNav()
})

function openNav () {
  if ($('#hide').css('display') === 'none') {
    element.style.display = 'block'
  } else {
    element.style.display = 'none'
  }
}

$('#updateButton').click(function (e) {
  e.preventDefault()
  // TODO: Error handling
  $.post('/update', function () {
    alert('Galerie hat neu geladen')
  })
})

$('.deleteImage').click(function (e) {
  e.preventDefault()
  $.post('/delete', { id: this.name }, function () {})
})
