/*
var path = require('path')
*/
var config = require('../../config')
var fs = require('fs')
/*
var Dropbox = require('dropbox')
var mkdirp = require('mkdirp')
var yaml = require('yamljs')
*/
var cloudinary = require('cloudinary')

cloudinary.config({
  cloud_name: config.cloudName,
  api_key: config.apiKey,
  api_secret: config.apiSecret
})

var data = {
  'paintings': [],
  'sketches': []
}

function downloadImages () {
  data.paintings = []
  data.sketches = []
  console.log("downloading...")
  cloudinary.v2.api.resources({type: 'upload', context: 'true', prefix: 'paintings/'}, function (error, result) {
    result.resources.forEach(function (entry, key) {
      entry.context.custom.tag = cloudinary.image(entry.public_id)
      entry.context.custom.folder = entry.context.custom.title.toLowerCase()
      entry.context.custom.pubId = entry.public_id

      data.paintings[key] = entry.context.custom
    })
    data.paintings.sort(function (a, b) {
      var c = new Date(a.date)
      var d = new Date(b.date)
      return c > d ? -1 : c < d ? 1 : 0
    })
  })
  cloudinary.v2.api.resources({type: 'upload', context: 'true', prefix: 'sketches/'}, function (error, result) {
    result.resources.forEach(function (entry, key) {
      entry.context.custom.tag = cloudinary.image(entry.public_id)
      entry.context.custom.folder = entry.context.custom.title.toLowerCase()
      entry.context.custom.pubId = entry.public_id

      data.sketches[key] = entry.context.custom
    })
    data.sketches.sort(function (a, b) {
      var c = new Date(a.date)
      var d = new Date(b.date)
      return c > d ? -1 : c < d ? 1 : 0
    })
    // console.log(error)
  })
}

function uploadImages (path, data) {
  console.log(data)

  cloudinary.v2.uploader.upload(path, {
    folder: data.folder,
    use_filename: true,
    context: data
  }, function (error, result) {
    console.log(result, error)
  })
  fs.unlink(path)
}

function deleteImage (url) {
  console.log(url)
  cloudinary.uploader.destroy(url, function (result) { console.log(result) })
}

//downloadImages()

var exportedData = data
module.exports = {
  data: exportedData,
  download: function () {
    downloadImages()
  },
  upload: function (path, data) {
    uploadImages(path, data)
  },
  delete: function (id) {
    deleteImage(id)
  }
}
